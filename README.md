# Express.js

File (index.js) adalah sebuah file yang berisi implementasi dari
HTTP server yang ditulis menggunakan Express.js.

Untuk menjalankan HTTP server tersebut, cukup jalankan perintah

1. Install dependency

   ```bash
   # Pengguna NPM
   npm init -y
   npm install express
    npm install -g nodemon

2. Jalankan server

   ```
   nodemon index.js
   ```

   # implementation
    -navigation bar 
      -ketika diklik home maka tampilan akan langsung diarahkan di daftar buku.
      - ketika diklik Contact Us akan langsung diarahkan di contact untuk menghubungi untuk menanyakan buku.
      - ketika diklik register akan langsung diarahkan ke form register.
    -action
        - ketika klik buy maka tampilan akan langsung diarahkan ke purchased
# Tech 
-EJS
- CSS
- Bootstrap 5


