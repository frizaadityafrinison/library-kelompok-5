const express = require('express');
const fs = require('fs');
const app = express();
const port = 3000;
const bookData = fs.readFileSync('./data/books.json', 'utf-8');
const books = JSON.parse(bookData);

app.use(express.static( 'views'));
app.use(express.urlencoded({extended:true}));
app.set('view engine', 'ejs');

app.get('/', (req, res) => {
    res.status(200).render('index', {data: books});
})

app.get('/register', (req, res) => {
    res.status(200).render('register');
})

app.get('/purchased', (req, res) => {
    res.status(200).render('purchased');
})

app.listen(port, function() {
    console.log(`http://localhost:${port}`);
})